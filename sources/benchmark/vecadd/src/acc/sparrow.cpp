#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <sparrow.h>
#include "accel.h"

int acc_init(accel_data_t *host, accel_data_t *acc)
{
    printf("SPARROW:\n    - %d lanes\n\n", sizeof(uword_t));

    // Dont need to do malloc for A and B nor transfer data
    acc->A = host->A;
    acc->B = host->B;

    // Allocate for result
    acc->C = (uint8_t*) malloc(acc->N * sizeof(uint8_t));

    if (! acc->C) return 1; // Error in malloc

    acc->accl_time = 0.0f;
    acc->accl_mem = 0;

    return 0;
}

int acc_exec(accel_data_t *data) 
{
    uint32_t N = data->N;

    clock_t t0 = clock();
    // Compute matrix multiplication
    for (int i = 0; i < N; i+=sizeof(uword_t)) {
        *((uword_t*) &data->C[i]) = vadd_u8(vget_1d(data->A, i), vget_1d(data->B, i));
    }
    clock_t t1 = clock();
    data->exec_time = ((double) (t1 - t0)) / (CLOCKS_PER_SEC / 1000);  

    //A, B, C (uint8_t)
    data->exec_mem = (N*3) * sizeof(uint8_t);

    return 0;
}

int acc_fetch(accel_data_t *data)
{
    data->host_time = 0.0f;
    data->host_mem = 0;
    // In CPU code no need to do anything
    return 0;
}

int acc_clean(accel_data_t *data)
{
    // Free allocated memory
    free(data->C);
    return 0;
}
