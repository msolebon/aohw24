#include <stdint.h>
#include <vx_intrinsics.h>
#include <vx_spawn.h>

typedef struct {
  uint32_t N;
  uint64_t A;
  uint64_t B;
  uint64_t C;  
} kernel_arg_t;

#define KERNEL_ARG_DEV_MEM_ADDR 0x5ffff000

void kernel(uint32_t task_id, kernel_arg_t* __UNIFORM__ arg)
{
    uint32_t  N = (uint32_t)  arg->N;
    uint8_t  *A = (uint8_t *) arg->A;
    uint8_t  *B = (uint8_t *) arg->B;
    uint8_t  *C = (uint8_t *) arg->C;

    // Compute Vector addition
    int i = task_id;
    C[i] = A[i] + B[i];
}

int main()
{
    kernel_arg_t* arg = (kernel_arg_t*) (KERNEL_ARG_DEV_MEM_ADDR);
    vx_spawn_tasks(arg->N, (vx_spawn_tasks_cb)kernel, arg);
}
