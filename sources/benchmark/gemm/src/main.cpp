#include <iostream>
#include <cstdlib>
#include <chrono>
#include "accel.h"

#define RT_ACCEL(_expr)                                         \
   do {                                                         \
     int _ret = _expr;                                          \
     if (0 == _ret)                                             \
       break;                                                   \
     printf("Error: '%s' returned %d!\n", #_expr, (int)_ret);   \
       host_clean(&host_data);                                  \
       acc_clean(&acc_data);                                    \
     exit(-1);                                                  \
   } while (false)

#define RT_MALLOC(_expr)                                        \
    do {                                                        \
        if (!_expr) {                                           \
            printf("Error: malloc(%s) failed!\n", #_expr);      \
            host_clean(data);                                   \
            exit(-1);                                           \
        }                                                       \
    } while (false)

void dcache_flush()
{
    uint64_t cctrl;
    asm("csrr %0, 0x7C1":"=r"(cctrl)::"memory");
    cctrl |= 0x08;
    asm("csrw 0x7C1, %0"::"r"(cctrl):"memory");
}

void host_clean(accel_data_t *host){
    free(host->A);
    free(host->B);
    free(host->C);
}

void gen_data(accel_data_t *data)
{
    uint32_t NM = data->N * data->M;
    uint32_t NN = data->N * data->N;

    //initialize A matrix
    data->A = (uint8_t*) malloc(sizeof(uint8_t) * NM);
    RT_MALLOC(data->A);
    for (int i = 0; i < NM; ++i) {
        float r = static_cast<float>(std::rand()) / RAND_MAX;
        int value = static_cast<int>(r * 256) - 128;
        data->A[i] = value;
    }

    //initialize B matrix 
    data->B = (uint8_t*) malloc(sizeof(uint8_t) * NM);
    RT_MALLOC(data->B);
    for (int i = 0; i < NM; ++i) {
        float r = static_cast<float>(std::rand()) / RAND_MAX;
        int value = static_cast<int>(r * 256) - 128;
        data->B[i] = value;
    }

#ifndef NVERIFY
    //initialize C matrix
    data->C = (uint32_t*) malloc(sizeof(uint32_t) * NN);
    RT_MALLOC(data->C);
    for (int i = 0; i < data->N; ++i) {
        for (int j = 0; j < data->N; ++j) {
            int sum = 0;
            for (int k = 0; k < data->M; ++k) {
                sum += data->A[i * data->M + k] * data->B[k * data->N + j];
            }
            data->C[i * data->N + j] = sum;
        }
    }
#endif
}


void verify(accel_data_t *host, accel_data_t *acc)
{
    int errors = 0;
    for (int i = 0; i < host->N * host->N; ++i) {
        if (host->C[i] != acc->C[i]) {
            if (errors < 5)
                std::cout << "error at result (" << std::dec << i/(host->N) <<"," << i%(host->N) << std::hex << "): actual=" << acc->C[i] << ", expected=" << host->C[i] << std::endl;
            else if (errors == 5) std::cout << "..." << std::endl;

            ++errors;
        }
    }
    if (errors != 0) {
        std::cout << "Found " << std::dec << errors << " errors!" << std::endl;
        std::cout << "FAILED!" << std::endl;
    }
    else std::cout << "PASSED!" << std::endl;
}

int main()
{
	srand(VALUE_N*VALUE_M);

	printf("Matrix multiplication:\n(%dx%d) x (%dx%d)\n\n",VALUE_N, VALUE_M, VALUE_M, VALUE_N);

        accel_data_t acc_data;
	acc_data.N = VALUE_N; acc_data.M = VALUE_M;

        accel_data_t host_data;
	host_data.N = VALUE_N; host_data.M = VALUE_M;

        // Generate data for A, B and compute reference data
        gen_data(&host_data);
        
        // Initialize the accelerator
        RT_ACCEL(acc_init(&host_data, &acc_data));

        // Flush cache before executing
        dcache_flush();
        // Accel execution
        RT_ACCEL(acc_exec(&acc_data));

        // Fetch result from accelerator
        RT_ACCEL(acc_fetch(&acc_data));

        // Verification
#ifndef NVERIFY
        verify(&host_data, &acc_data);
#endif
        //for(int i= 0; i< 5; i++) printf("%x\n", acc_data.C[i]);

        // Print timings
        printf("\n*** Time report ***\n");
        printf("Data transfer time to acc.: %lg ms\n", acc_data.accl_time);
        printf("Accelerator execution time: %lg ms\n", acc_data.exec_time);
        printf("Data transfer time to host: %lg ms\n", acc_data.host_time);
        printf("\n");
        printf("Total execution time:       %lg ms\n", acc_data.accl_time + acc_data.host_time + acc_data.exec_time);
        printf("\n");
        printf("\n*** Memory report ***\n");
        printf("Data transfer size to acc.: %d bytes\n", acc_data.accl_mem);
        printf("Data transfer size to host: %d bytes\n", acc_data.host_mem);
        printf("\n");
        printf("Total memory used:          %d bytes\n", acc_data.exec_mem);
        printf("\n");

        // Clean memory
        host_clean(&host_data);
        acc_clean(&acc_data);
}

