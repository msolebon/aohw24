#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <sparrow.h>
#include "accel.h"

int acc_init(accel_data_t *host, accel_data_t *acc)
{
    printf("SPARROW:\n    - %d lanes\n\n", sizeof(uword_t));

    // Dont need to do malloc for A and B nor transfer data
    acc->A = host->A;
    acc->B = host->B;

    // Allocate for transposed
    acc->T = (uint8_t*) malloc(acc->N * acc->M * sizeof(uint8_t));

    // Allocate for result
    acc->C = (uint32_t*) malloc(acc->N * acc->N * sizeof(uint32_t));

    if (!acc->T || ! acc->C) return 1; // Error in malloc
    acc->accl_time = 0.0f;
    acc->accl_mem = 0;

    return 0;
}

int acc_exec(accel_data_t *data) 
{
    uint32_t N = data->N;
    uint32_t M = data->M;

    clock_t t0 = clock();
    // Transpose B into T
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            data->T[j * M + i] = data->B[i * N + j];
        }
    }

    // Compute matrix multiplication
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            int sum = 0;
            for (int k = 0; k < M; k += sizeof(uword_t)) {
                sum += dot_u8(vget_1d(data->A, i * M + k), 
                              vget_1d(data->T, j * M + k));

            }
            data->C[i * N + j] = sum;
        }
    }
    clock_t t1 = clock();
    data->exec_time = ((double) (t1 - t0)) / (CLOCKS_PER_SEC / 1000);  

    //A, B, T (uint8_t) + C (uint32_t)
    data->exec_mem = (N*M*3) * sizeof(uint8_t) + (N*N) * sizeof(uint32_t);

    return 0;
}

int acc_fetch(accel_data_t *data)
{
    data->host_time = 0.0f;
    data->host_mem = 0;
    // In CPU code no need to do anything
    return 0;
}

int acc_clean(accel_data_t *data)
{
    // Free allocated memory
    free(data->T);
    free(data->C);
    return 0;
}
