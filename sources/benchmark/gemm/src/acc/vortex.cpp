#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <vortex.h>
#include "accel.h"
#include "vx_kernel.h"

#define RT_CHECK(_expr)                                         \
   do {                                                         \
     int _ret = _expr;                                          \
     if (0 == _ret)                                             \
       break;                                                   \
     printf("Error: '%s' returned %d!\n", #_expr, (int)_ret);   \
     return 1;                                                  \
   } while (false)


int acc_init(accel_data_t *host, accel_data_t *acc) {

    puts("Vortex GPU:");

    // Create the device object and store it in the acc_data
    vx_device_h *device = (vx_device_h*) malloc(sizeof(vx_device_h));
    RT_CHECK(vx_dev_open(device));
    acc->device = device;
    
    // Get the device capabilities and print init message
    uint64_t nwarps, nthreads, ncores;
    RT_CHECK(vx_dev_caps(*device, VX_CAPS_NUM_THREADS, &nthreads));
    RT_CHECK(vx_dev_caps(*device, VX_CAPS_NUM_WARPS, &nwarps));
    RT_CHECK(vx_dev_caps(*device, VX_CAPS_NUM_CORES, &ncores));
    printf("    - %ld cores\n    - %ld warps\n    - %ld threads\n\n", ncores, nwarps, nthreads);

    // Upload the kernel to the device
    RT_CHECK(vx_upload_kernel_bytes(*device, build_vx_kernel_bin, build_vx_kernel_bin_len));

    // Prepare the kernel arguments
    kernel_arg_t *kernel_arg = (kernel_arg_t*) malloc(sizeof(kernel_arg_t));
    acc->arg = kernel_arg;

    RT_CHECK(vx_mem_alloc(*device, host->N * host->M * sizeof(uint8_t), 
                VX_MEM_TYPE_GLOBAL, &kernel_arg->A));
    RT_CHECK(vx_mem_alloc(*device, host->N * host->M * sizeof(uint8_t),
                VX_MEM_TYPE_GLOBAL, &kernel_arg->B));
    acc->exec_mem = host->N * host->M * sizeof(uint8_t) * 2;
    RT_CHECK(vx_mem_alloc(*device, host->N * host->N * sizeof(uint32_t),
                VX_MEM_TYPE_GLOBAL,  &kernel_arg->C));
    acc->exec_mem += host->N * host->N * sizeof(uint32_t);

    kernel_arg->N = acc->N;
    kernel_arg->M = acc->M;
    
    // Upload the argument to the GPU
    RT_CHECK(vx_copy_to_dev(*device, KERNEL_ARG_DEV_MEM_ADDR, 
                kernel_arg, sizeof(kernel_arg_t)));

    clock_t t0 = clock();
    // Upload source data
    RT_CHECK(vx_copy_to_dev(*device, kernel_arg->A, host->A, acc->N * acc->M * sizeof(uint8_t)));
    RT_CHECK(vx_copy_to_dev(*device, kernel_arg->B, host->B, acc->N * acc->M * sizeof(uint8_t)));

    clock_t t1 = clock();
    acc->accl_time = ((double) (t1 - t0)) / (CLOCKS_PER_SEC / 1000);
    //A, B
    acc->accl_mem = (host->N * host->M * sizeof(uint8_t))*2;

    // Alocate buffer for result data
    acc->C = (uint32_t*) malloc(acc->N * acc->N * sizeof(uint32_t));

    return 0;
}

int acc_exec(accel_data_t *data) 
{
    vx_device_h device = *data->device;
    
    clock_t t0 = clock();
    // Start execution
    RT_CHECK(vx_start(device));

    // Wait for execution to finish
    RT_CHECK(vx_ready_wait(device, VX_MAX_TIMEOUT));
    clock_t t1 = clock();
    data->exec_time = ((double) (t1 - t0)) / (CLOCKS_PER_SEC / 1000);  

    return 0;
}

int acc_fetch(accel_data_t *data)
{
    vx_device_h device = *data->device;
    kernel_arg_t kernel_arg = *data->arg;

    clock_t t0 = clock();
    RT_CHECK(vx_copy_from_dev(device, data->C, kernel_arg.C, data->N * data->N * sizeof(uint32_t)));
    clock_t t1 = clock();
    data->host_time = ((double) (t1 - t0)) / (CLOCKS_PER_SEC / 1000);  
    data->host_mem = data->N * data->N * sizeof(uint32_t);
    return 0;
}

int acc_clean(accel_data_t *data)
{
    vx_device_h device = *data->device;
    kernel_arg_t kernel_arg = *data->arg;

    vx_mem_free(device, kernel_arg.A);
    vx_mem_free(device, kernel_arg.B);
    vx_mem_free(device, kernel_arg.C);
    vx_dev_close(device);

    // Free allocated memory
    free(data->C);
    free(data->arg);
    free(data->device);
    return 0;
}

