#include <stdint.h>
#include <vx_intrinsics.h>
#include <vx_spawn.h>

typedef struct {
  uint32_t N;
  uint32_t M;
  uint64_t A;
  uint64_t B;
  uint64_t C;  
} kernel_arg_t;

#define KERNEL_ARG_DEV_MEM_ADDR 0x5ffff000

void kernel(uint32_t task_id, kernel_arg_t* __UNIFORM__ arg)
{
    uint32_t  N = (uint32_t)   arg->N;
    uint32_t  M = (uint32_t)   arg->M;
    uint8_t  *A = (uint8_t *)  arg->A;
    uint8_t  *B = (uint8_t *)  arg->B;
    uint32_t *C = (uint32_t *) arg->C;

    // Compute matrix multiplication
    int i = task_id;
    for (int j = 0; j < N; ++j) {
        uint32_t sum = 0;
        for (int k = 0; k < M; ++k) {
            sum += A[i*M+k] * B[k*N+j];
        }
        C[i * N + j] = sum;
    }

}

int main()
{
    kernel_arg_t* arg = (kernel_arg_t*) (KERNEL_ARG_DEV_MEM_ADDR);
    vx_spawn_tasks(arg->N, (vx_spawn_tasks_cb)kernel, arg);
}
