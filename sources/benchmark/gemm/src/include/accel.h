#ifndef _ACCEL_H_
#define _ACCEL_H_

#include <cstdint>

#ifdef ACCEL_sparrow
#endif
#ifdef ACCEL_cpu
#endif
#ifdef ACCEL_vortex

#include<vortex.h>

typedef struct {
  uint32_t N;
  uint32_t M;
  uint64_t A;
  uint64_t B;
  uint64_t C;  
} kernel_arg_t;

#define KERNEL_ARG_DEV_MEM_ADDR 0x5ffff000

#endif

typedef struct {
    //sizes
    uint32_t N;
    uint32_t M;
    //pointers
    uint8_t  *A;
    uint8_t  *B;
    uint8_t  *T;
    uint32_t *C;
    //time info
    double accl_time;
    double exec_time;
    double host_time;
    //memory info
    uint32_t accl_mem;
    uint32_t exec_mem;
    uint32_t host_mem;
#ifdef ACCEL_vortex
    vx_device_h *device;
    kernel_arg_t *arg;
#endif

} accel_data_t;

int acc_init(accel_data_t *host, accel_data_t *acc);
int acc_exec(accel_data_t *data);
int acc_fetch(accel_data_t *data);
int acc_clean(accel_data_t *data);


#endif
