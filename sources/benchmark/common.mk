# NOEL-V Compiler
GCC_PREFIX ?= riscv-gaisler-elf-
CXX = $(GCC_PREFIX)g++
CC  = $(GCC_PREFIX)gcc
OD  = $(GCC_PREFIX)objdump

# Compiler flags
CXXFLAGS += -I$(IDIR) -I$(BDIR) -O$(OFLAG)
CFLAGS += -I$(IDIR) -I$(BDIR) -O$(OFLAG)

# Vortex Compiler
RISCV_TOOLCHAIN_PATH = /home/msole/metasat/software/tools/compiler/riscv-vortex-toolchain
RISCV_PREFIX = riscv32-unknown-elf

VX_CC  = $(RISCV_TOOLCHAIN_PATH)/bin/$(RISCV_PREFIX)-gcc
VX_CXX = $(RISCV_TOOLCHAIN_PATH)/bin/$(RISCV_PREFIX)-g++
VX_DP  = $(RISCV_TOOLCHAIN_PATH)/bin/$(RISCV_PREFIX)-objdump
VX_CP  = $(RISCV_TOOLCHAIN_PATH)/bin/$(RISCV_PREFIX)-objcopy

VORTEX_KN_PATH = ../../extra/vortex/kernel
VORTEX_RT_PATH = ../../extra/vortex/runtime

# VX compiler flags
VX_XLEN = 32
VX_STARTUP_ADDR = 0x60000000
VX_CFLAGS += -O$(OFLAG) -std=c++17
VX_CFLAGS += -mcmodel=medany -fno-rtti -fno-exceptions -nostartfiles -fdata-sections -ffunction-sections
VX_CFLAGS += -I$(VORTEX_KN_PATH)/include -I$(VORTEX_KN_PATH)/../hw -I$(IDIR)
VX_CFLAGS += -DNDEBUG -DACCEL_=vortex

VX_LDFLAGS += -Wl,-Bstatic,--gc-sections,-T,$(VORTEX_KN_PATH)/linker/vx_link$(VX_XLEN).ld,--defsym=STARTUP_ADDR=$(VX_STARTUP_ADDR) $(VORTEX_KN_PATH)/libvortexrt.a

# Directories
SDIR := src
IDIR := $(SDIR)/include

BDIR := build
ODIR ?= .


OBJ = $(patsubst %.c,%.c.o,$(patsubst %.cpp,%.cpp.o,$(addprefix $(BDIR)/,$(SRC))))

# RULES
$(BDIR): 
	mkdir -p $(BDIR)/$(EXTRA_DIR)

.PHONY: $(SDIR)/main.cpp

$(BDIR)/%.c.o: $(SDIR)/%.c $(BDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BDIR)/%.cpp.o: $(SDIR)/%.cpp $(BDIR)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(TEST)-$(MAKECMDGOALS): $(OBJ)
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS) -o $(ODIR)/$@
	$(OD) $(ODIR)/$@ -DC > $(BDIR)/$@.dump

# VX kernel rules
%.h: %.bin
	xxd -i $^ > $@

%.bin: %.elf
	$(VX_CP) -O binary $^ $@

%.elf: $(VX_SRCS) $(BDIR)
	$(VX_CXX) $(VX_CFLAGS) $(VX_SRCS) $(VX_LDFLAGS) -o $@

# Clean rules
clean:
	rm -rf $(BDIR)

clean-all: clean
	rm -rf $(ODIR)/$(TEST)*

