#  AMD Open Hardware 2024 - Embedded SoC with a RISC-V GPU for Critical Systems

## Project information 

Team number: 122

Project name: Embedded SoC with a RISC-V GPU for Critical Systems

Link to YouTube Video(s): [Video](https://youtu.be/Qt3TvHiYYVM)

University name: Universitat Politècnica de Catalunya (UPC)

Participant(s): Marc Solé Bonet

Email: marc.sole.bonet@estudiantat.upc.edu

Supervisor name: Leonidas Kosmidis

Supervisor e-mail: leonidas.kosmidis@bsc.es


## Description of the project

The objective of the work presented is to provide a high performance system for critical applications prototyped on an FPGA.
In particular, this project focuses on the integration of an existing RISC-V GPU (Vortex) designed as a discrete GPU for high performance systems with a RISC-V space processor (FrontGrade Gaisler’s NOEL-V).
In this way, we produce an embedded SoC platform with an integrated GPU.
In addition to hardware, we also describe the porting and the evaluation of its software stack, so that the embedded GPU can be used from the RISC-V multicore softcore, either in bare metal or from a real-time operating system.
 

## File organization
AOHW24:
 - [releases](releases/): Directory containing all pre-compiled applications and bitstream
     - [bin](releases/bin): Pre-compiled applications to be executed on the platform
     - [bitstream](releases/bitstream): Synthesized design of the platform
     - [logs](releases/logs): Logs from the execution of the different evaluation tests
 - [sources](sources/): Directory containing all the files to build the project from scratch
     - [benchmark](sources/benchmark): Sources for some tests to evaluate the platform
     - [extra/vortex](sources/extra/vortex): All the source files related to the GPU
     	- [hw](sources/extra/vortex/hw): Hardware files for the implementation of the GPU
     	- [kernel](sources/extra/vortex/kernel): Library for the GPU code
     	- [runtime](sources/extra/vortex/runtime): Driver for the host to use the GPU
     	- [tests/regression](sources/extra/vortex/tests/regression): Diferent tests to verify the GPU execution
     - [grlib/lib](sources/grlib/lib): Hardware files for the multicore part of the platform
     - [metasat/metasat-xilinx-vcu118](sources/metasat/metasat-xilinx-vcu118): Top level files for generating the bitstream


## Instructions to build and test project

All the steps are explained for a Linux system, tested on Ubuntu 18.04.6 LTS.
This project has been developed on a Virtex Ultrascale+ (VCU118) and using Vivado 2020.2 version.

### Generating the bitstream
Requirements:
 - Vivado 2020.2

Step 1: Go to "[metasat/metasat-xilinx-vcu118](sources/metasat/metasat-xilinx-vcu118)" directory.

Step 2: Execute `make metasat-synth` to generate the bitstream `metasat.bit`.

(Optional: Before running Vivado, modify the [config.vhd](sources/metasat/metasat-xilinx-vcu118/config.vhd) and [vx_config.inc](sources/metasat/metasat-xilinx-vcu118/vx_config.inc) files to configure the CPU and GPU respectively.)

### Compile the GPU kernel library
Requirements: 
 - [RISC-V compiler (generic ELF)](https://github.com/riscv-collab/riscv-gnu-toolchain)

Step 1: Go to "[extra/vortex/kernel](sources/extra/vortex/kernel)" directory.

Step 2: Run `make RISCV_TOOLCHAIN_PATH=/path/to/riscv/toolchain`.

(Optional: If using LLVM, modify the [Makefile](sources/extra/vortex/kernel/Makefile) appropriately.)

### Get the NOEL-V cross compiler

Option 1: Compile from sources from [here](https://gitlab.bsc.es/msolebon/ncc-sparrow).

Option 2: Download the binaries (NCC GNU GCC C/C++ bare-metal toolchain) from [here](https://www.gaisler.com/index.php/downloads/sw-noelv-downloads).
With this option SPARROW code can't be compiled.

### Compile the GPU driver library
Requirements: 
 - Bitstream generated or at least vortex files prepared (`make vortex` in [metasat/metasat-xilinx-vcu118](sources/metasat/metasat-xilinx-vcu118)
 - NOEL-V cross compiler

Step 1: Go to "[extra/vortex/runtime/soc](sources/extra/vortex/runtime/soc)" dirctory.

Step 2: Do `make CXX=/path/to/NOELV/cross-compiler/riscv-gaisler-elf-g++ AR=/path/to/NOELV/cross-compiler/riscv-gaisler-elf-ar`.

### Compile the tests
Requirements: 
 - NOEL-V cross compiler
 - RISC-V compiler
 - GPU kernel library
 - GPU driver library

Step 1: Go to "[benchmark/](sources/benchmark)" directory.

Step 2: Modify "[common.mk](sources/benchmark/common.mk)" and set GCC_PREFIX to your NOEL-V Cross Compiler and RISCV_TOOLCHAIN_PATH to point to the generic ELF RISC-V toolchain.

Step 3: Go to any of the existing subdirectories.

Step 4: Run `make cpu|sparrow|vortex` to generate code for the NOEL-V, for SPARROW or for the GPU respectively or just do `make` to compile all the versions.
You can use `ARG_N` to set the size for the tests (also `ARG_M` for the second matrix dimension in the gemm (matrix multiplication) test).
Setting `ARG_V` to any value will also run a verification step to check if the output result is correct.
The command used to compile the gemm test provided in the [releases](releases/bin/gemm/) is:

```bash
make ARG_N=256 ARG_M=256 ARG_V=1
```

### Execute on the FPGA
Requirements:
 - Bitstream generated
 - Any of the test compiled
 - [GRMON 3 Evaluation/Academic version](https://www.gaisler.com/index.php/downloads/debug-tools)

Step 1: Run `grmon -digilent -jtaglist` to check the id of the FPGA.

Step 2: Do `grmon -digilent -jtagcable id_num -u` where id_num is the id identified in the previous step.

Step 3: From the GRMON terminal `load test-to-execute` for any test to try.

Step 3.1: If running an RTEMS program do `dtb /path/to/repo/releases/bin/rtems/dtb/metasat_cpu_soc.dtb` to load the [DTB](releases/bin/rtems/dtb/metasat_cpu_soc.dtb).

Step 3.2: If running an RTEMS program do `forward enable uart2`.

Step 4: Run the test with `run`.
